﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableShip : MonoBehaviour
{
    private GameObject myShip;  //Create variable for game object to set to inactive
    private SpriteMover mover;  //Create variable for component to disable

    //Intialization
    void Start()
    {
        myShip = this.gameObject;
        mover = GetComponent<SpriteMover>();  //Get access to component
    }

    //Update is called once per frame
    void Update()
    {
        //If player presses P key, toggle movemnt component on and off.
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (mover.enabled == true)
            {
                mover.enabled = false;
            }
        else { mover.enabled = true; }
        }
        //If player presses Q key, set game object to inactive
        if (Input.GetKeyDown(KeyCode.Q))
        {
            myShip.SetActive(false);
        }
    }
}
