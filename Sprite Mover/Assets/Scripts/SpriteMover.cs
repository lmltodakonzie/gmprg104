﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteMover: MonoBehaviour
{
    private Transform tf;  //Create variable for transform component
    public float speed = 0.25f;  //Create variable for speed

    //Use this for initialization. Start is called before the first frame update.
    void Start()
    {
        tf = GetComponent<Transform>(); //Access to transform component
    }
    //Update called once per frame
    void Update()
    {
        //check inputs from player
        //if player presses arrow key, move continuously in that direction
        
        if (Input.GetAxis("horizontal") > 0 && !Input.GetButton("step"))  //Move right
        {
            tf.position += (new Vector3(1, 0, 0) * speed);
        }
        else if (Input.GetAxis("horizontal") < 0 && !Input.GetButton("step"))  //Move left
        {
            tf.position -= (new Vector3(1, 0, 0) * speed);
        }
        if (Input.GetAxis("vertical") > 0 && !Input.GetButton("step"))  //Move upward
        {
            tf.position += (new Vector3(0, 1, 0) * speed);
        }
        else if (Input.GetAxis("vertical") < 0 && !Input.GetButton("step"))  //Move downward
        {
            tf.position -= (new Vector3(0, 1, 0) * speed);
        }
        //If player presses shift + arrow key, move one unit in that direction
        if (Input.GetButton("step") && Input.GetKeyDown(KeyCode.RightArrow))  //Move Right
        {
            tf.position += new Vector3(1, 0, 0);
        }
        else if (Input.GetButton("step") && Input.GetKeyDown(KeyCode.LeftArrow))  //Move Left
        {
            tf.position -= new Vector3(1, 0, 0);
        }
        else if (Input.GetButton("step") && Input.GetKeyDown(KeyCode.UpArrow))  //Move up
        {
            tf.position += new Vector3(0, 1, 0);
        }
        else if (Input.GetButton("step") && Input.GetKeyDown(KeyCode.DownArrow))  //Move down
        {
            tf.position -= new Vector3(0, 1, 0);
        }
        //If the player presses space bar, move to center
        if (Input.GetKeyDown(KeyCode.Space))
        {
            tf.position = Vector3.zero;
        }
    }
}
