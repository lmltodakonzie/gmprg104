﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitGame : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        //If escape button is pressed, exit program
        if (Input.GetButtonDown("Quit"))
        {
            Application.Quit();
        }
    }
}
